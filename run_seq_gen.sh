theta=$1
for i in output'_'$theta/*;
do
  filename=$(basename "$i")
  echo $filename
  IFS='_' read -a array <<< "$filename"
  seq-gen -mHKY -l 1000 -s $theta < $i > output'_'$theta/seq_${array[1]}_${array[2]}_${array[3]}
done

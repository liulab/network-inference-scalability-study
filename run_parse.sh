taxa=$1
theta=$2
height=$3
mkdir $height/output'_'$theta/_$taxa
for i in `seq 1 20`;
do
  fname=$height/output'_'$theta/seq'_'$height'_'$taxa'_'$i.txt
  mkdir $height/output'_'$theta/_$taxa/$i
  python parse.py $fname $taxa $i $theta $height
  rm $height/output'_'$theta/_$taxa/$i/g_$taxa'_'$i'_'1001.fasta
  for j in `seq 1 1000`;
  do
    FastTree -nt -quiet $height/output'_'$theta/_$taxa/$i/g_$taxa'_'$i'_'$j.fasta >> $height/output'_'$theta/_$taxa/$taxa'_'$i.tree
  done
done

import sys
import csv

file = str(sys.argv[1])
print file

taxa = int(sys.argv[2])
print taxa

replicate = int(sys.argv[3])
print replicate

theta = str(sys.argv[4])
print theta

height = str(sys.argv[5])
print height

stop = int(taxa) + 1 + 1
seq = open(file, "r")
counter = 0
g = 1
filename = str(height) + "/output_" + theta + "/_" + str(taxa) + "/" + str(replicate) + "/g_" + str(taxa) + "_" + str(replicate) + "_" + str(g) + ".fasta"
target = open(filename, 'w')

for i in iter(seq):
  i = i.strip()
  data = i.split()
  if counter!=0:
    target.write(">")
    target.write(data[0])
    target.write("\n")
    target.write(data[1])
    target.write("\n")
  counter = counter + 1
  if counter%stop==0:
    counter = 0
    g = g + 1
    filename = str(height) + "/output_" + theta + "/_" + str(taxa) + "/" + str(replicate) + "/g_" + str(taxa) + "_" + str(replicate) + "_" + str(g) + ".fasta"
    target = open(filename, 'w')
